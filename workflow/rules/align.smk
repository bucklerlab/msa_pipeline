from snakemake import available_cpu_count
from psutil import virtual_memory

# lastal parameters may now be specified in the config file by
# editing the "alignParams" parameter.

available_mem_gb = lambda: '%dG' % (virtual_memory().available >> 30)
containerized: "docker://apscheben/msa_pipeline:latest"

SPECIES = config['species']

# split fasta index numbers
splitN = config["splitFastaN"]
padN = len(str(splitN))
Nlist = list(range(0, splitN))
padList = [str(item).zfill(padN) for item in Nlist]

# Prevent evaluation of lastParams as None
if not config["lastParams"]:
    LAST_PARAMS = ""
else:
    LAST_PARAMS = config["lastParams"]
if not config["lastSplit"]:
    config["lastSplit"] = ""

# Only allow split aligments with last aligner
if not config["splitFastaN"]:
    config["splitFastaN"] = 1
elif config["splitFastaN"] > 1 and config["aligner"] != "last":
    config["aligner"] = "last"

# Set empty params to default values
if not config["lastParams"]:
    config["lastParams"] =  "-m 10 -j 3 -u 1 -p HOXD70"
if not config["minimap2Params"]:
    config["minimap2Params"] = "-a -cx asm20"
if not config["gsalignParams"]:
    config["gsalignParams"] = "-sen -no_vcf"
if not config["roastParams"]:
    config["roastParams"] = "+ X=2 E="

# Ensure split alignment rule is prioritised when needed
if config["aligner"] == "gsalign":
    ruleorder: gsalign_index > lastdb_index
    ruleorder: align_single_gsalign > align_single_last > align_single_gsalign > align_split
elif config["aligner"] == "minimap2":
    ruleorder: lastdb_index > gsalign_index
    ruleorder: align_single_minimap > align_single_last > align_single_gsalign > align_split
elif config["aligner"] == "last" and config["splitFastaN"] == 1:
    ruleorder: lastdb_index > gsalign_index
    ruleorder: align_single_last > align_single_minimap > align_single_gsalign > align_split
else:
    ruleorder: lastdb_index > gsalign_index
    ruleorder: align_split > align_single_last > align_single_minimap > align_single_gsalign 

# This is the final output, and the rule to call to run all rules in this file
rule align_all:
    input:
      # the wildcards in psls will also handle definition of wildcards for fastas
      psls=expand("results/psl/{species}.psl",species=SPECIES)


rule lastdb_index:
    input:
      fasta='data/{refname}.fa'
    output:
      # Create a dummy file that tells us indexing the ref has finished.
      # file is actually created in the shell: directive with "touch"
      # This rule will also create the ref's 2bit file, which may be
      # used later in net_to_axt (but isn't used at the time of writing)
      temp('results/genome/{refname}lastdb_index.done')
    params:
      indexBase='data/{refname}',
      refSizeFile='results/genome/{refname}.size',
    log:
      'logs/{refname}_lastdbIndex_log.txt'
    threads: 2
    benchmark:
      'benchmark/{refname}-lastdb.txt'
    conda:
      '../envs/align.yaml'
    shell:
      """
      faSize -detailed {input.fasta} > {params.refSizeFile} 2>{log} && lastdb -R 10 -u YASS -c {params.indexBase} {input.fasta} 2>{log} && touch {output} 2>{log}
      """

rule gsalign_index:
    input:
      fasta='data/{refname}.fa'
    output:
      # Create a dummy file that tells us indexing the ref has finished.
      # file is actually created in the shell: directive with "touch"
      # This rule will also create the ref's 2bit file, which may be
      # used later in net_to_axt (but isn't used at the time of writing)
      temp('results/genome/{refname}lastdb_index.done')
    params:
      indexBase='data/{refname}',
      refSizeFile='results/genome/{refname}.size',
    log:
      'logs/{refname}_lastdbIndex_log.txt'
    threads: 2
    benchmark:
      'benchmark/{refname}-lastdb.txt'
    conda:
      '../envs/align.yaml'
    shell:
      """
      faSize -detailed {input.fasta} > {params.refSizeFile} 2>{log} && GSAlign index {input.fasta} {params.indexBase} 2>{log} && touch {output} 2>{log}
      """

rule build_index:
    input:
      str(rules.lastdb_index.output).format(refname=config['refName']),
      fastaFile="data/{species}.fa"
    output:
      "results/genome/{species}.size"
    params:
      indexBase='data/{refname}'.format(refname=config['refName']),
      speciesSizeFile='results/genome/{species}.size',
      refNibDir='results/genome/nib',
      refFastaFile='data/{refname}.fa'.format(refname=config['refName']),
      refNib2Bit='results/genome/nib/{refname}.2bit'.format(refname=config['refName']),
    log:
      'logs/{species}_index_log.txt'
    benchmark:
      'benchmark/{species}-index_bm.txt'
    conda:
      '../envs/ucsc.yaml'
    threads: 1
    shell:
      """
      mkdir -p {params.refNibDir} && \
      faToTwoBit {params.refFastaFile} {params.refNib2Bit} && \
      faSize -detailed {input.fastaFile} > {output}
      """

rule split_fasta:
    input:
      str(rules.lastdb_index.output).format(refname=config['refName']),
      speciesSizeFile='results/genome/{species}.size',
      fastaFile='data/{species}.fa' 
    output:
      splitFa=temp(expand('data/{{species}}.{index}.fa',index=padList)),
      flat=temp('data/{species}.fa.flat'),
      gdx=temp('data/{species}.fa.gdx'),
      splitDummy=temp('data/{species}.split')
    params:
      splitFastaN=config['splitFastaN']
    log:
      'logs/{species}_fasplit_log.txt'
    benchmark:
      'benchmark/{species}-split_bm.txt'
    conda:
      '../envs/split.yaml'
    threads: 1
    shell:
      """
      pyfasta split -n {params.splitFastaN} {input.fastaFile} &>{log} && \
      touch {output.splitDummy}
      """

rule align_single_last:
    input:
      str(rules.lastdb_index.output).format(refname=config['refName']),
      fastaFile="data/{species}.fa",
      speciesSizeFile='results/genome/{species}.size',
    output:
      name='results/psl/{species}.psl'
    params:
      indexBase='data/{refname}'.format(refname=config['refName']),
      lastParams=LAST_PARAMS,
      lastSplitParams=config['lastSplit'],
    log:
      'logs/{species}_align_log.txt'
    benchmark:
      'benchmark/{species}-align_bm.txt'
    conda:
      '../envs/align.yaml'
    threads: 1
    shell:
      # This shell will kick off for each fasta in the {genomedir}/fastas folder.  Each instance
      # of this rule gets 1 thread, but multiple lastal commands may be run, depending on the number of species
      # and the number of threads given on the command line.
      # NOTE: more threads means more memory used, and you could run out, so have to
      # temper the number of threads.
      # the file size from faSize is needed is the chain/net steps later as is the
      # ref .2bit file
      """
      lastal {params.lastParams} {params.indexBase} {input.fastaFile} {params.lastSplitParams} | maf-convert psl /dev/stdin 2>{log} 1>{output.name}
      """

rule align_single_minimap:
    input:
      str(rules.lastdb_index.output).format(refname=config['refName']),
      fastaFile="data/{species}.fa",
      speciesSizeFile='results/genome/{species}.size',
    output:
      name='results/psl/{species}.psl'
    params:
      minimap2Params=config['minimap2Params'],
      refFastaFile='data/{refname}.fa'.format(refname=config['refName']),
    log:
      'logs/{species}_align_log.txt'
    benchmark:
      'benchmark/{species}-align_bm.txt'
    conda:
      '../envs/align.yaml'
    threads: 1
    shell:
      # This shell will kick off for each fasta in the {genomedir}/fastas folder.  Each instance
      # of this rule gets 1 thread, but multiple lastal commands may be run, depending on the number of species
      # and the number of threads given on the command line.
      # NOTE: more threads means more memory used, and you could run out, so have to
      # temper the number of threads.
      # the file size from faSize is needed is the chain/net steps later as is the
      # ref .2bit file
      """
      minimap2 {params.minimap2Params} {params.refFastaFile} {input.fastaFile} 2>>{log} | samtools sort 2>>{log} | bamToPsl /dev/stdin {output.name} &>>{log}
      """

rule align_single_gsalign:
    input:
      str(rules.lastdb_index.output).format(refname=config['refName']),
      fastaFile="data/{species}.fa",
      speciesSizeFile='results/genome/{species}.size',
    output:
      name='results/psl/{species}.psl'
    params:
      indexBase='data/{refname}'.format(refname=config['refName']),
      speciesPath='results/genome/{species}',
      gsalignParams=config['gsalignParams'],
    log:
      'logs/{species}_align_log.txt'
    benchmark:
      'benchmark/{species}-align_bm.txt'
    conda:
      '../envs/align.yaml'
    threads: 1
    shell:
      # This shell will kick off for each fasta in the {genomedir}/fastas folder.  Each instance
      # of this rule gets 1 thread, but multiple lastal commands may be run, depending on the number of species
      # and the number of threads given on the command line.
      # NOTE: more threads means more memory used, and you could run out, so have to
      # temper the number of threads.
      # the file size from faSize is needed is the chain/net steps later as is the
      # ref .2bit file
      """
      GSAlign {params.gsalignParams} -r {params.refFastaFile} -q {input.fastaFile} -o {params.speciesPath} -i {params.indexBase} &>>{log} && \
      sed -i 's/^s qry\./s /' {params.speciesPath}.maf && sed -i 's/^s ref\./s /' {params.speciesPath}.maf && \
      maf-convert psl {params.speciesPath}.maf 2>>{log} 1>{output.name}
      """

rule align_split:
    input:
      splitFa=expand("data/{{species}}.{index}.fa",index=padList),
      splitDummy='data/{species}.split',
      speciesSizeFile='results/genome/{species}.size'
    output:
      psl='results/psl/{species}.psl',
      cmd=temp('results/genome/{species}.cmd'),
      cmdcomp=temp('results/genome/{species}.cmd.completed'),
      splitMaf=temp(expand('results/genome/{{species}}.{index}.maf',index=padList))
    params:
      indexBase='data/{refname}'.format(refname=config['refName']),
      refName=config['refName'],
      splitDir='results/genome/',
      speciesPath='results/genome/{species}',
      lastParams=LAST_PARAMS,
      lastSplitParams=config['lastSplit'],
      splitFa=expand('data/{{species}}.{index}.fa',index=padList)
    log:
      'logs/{species}_lastAlign_split_log.txt'
    benchmark:
      'benchmark/{species}_lastAlign_split_bm.txt'
    conda:
      '../envs/align.yaml'
    threads: config["splitFastaN"]
    shell:
      # This script will align split fasta files to the reference using parafly parallelization
      # It uses some potentially unsafe globbing and rm
      # These should be replaced with expand() inputs by eliminating 0 padding from split fasta names
      """
      ls {params.splitFa}| sed 's@.*/@@'| while read l; do echo "lastal {params.lastParams} {params.indexBase} data/$l > {params.splitDir}${{l%%.fa}}.maf";done > {params.speciesPath}.cmd && \
      ParaFly -c {params.speciesPath}.cmd -CPU {threads} &>{log} && \
      sed '30,${{/^#/d;}}' {output.splitMaf} | maf-sort /dev/stdin {params.lastSplitParams} | maf-convert psl /dev/stdin |awk '$9!="++"' > {output.psl}
      """
